﻿Koji je planet najtopliji?,Jupiter,Venera,Mars,Saturn,Venera
Koja je SI mjerna jedinica za otpor?,Amper,Om,Volt,Watt,Om
Koliko zakona termodinamike postoji?,2,3,4,5,3
Koji je planet veličinom najbliži Zemlji?,Merkur,Mars,Uran,Venera,Venera
Koji je znanstvenik uzviknuo "Eureka!"?,Arhimed,Pitagora,Tales,Empedoklo,Arhimed
Kojim dijelom tijela se bavi nefrologija?,Plućima,Bubrezima,Štitnjačom,Srcem,Plućima
Pozitron je antičestica:,Neutrona,Protona,Elektrona,Neurona,Elektrona
Zakon akcije i reakcije još se naziva i Newtonov:,1. zakon, 2.zakon, 3. zakon, 4. zakon, 3. zakon
Kako se naziva pravac koji krivulju dodiruje u samo jednoj točki?,Hipotenuza,Simetrala,Promjer,Tangenta,Tangenta
Koja je hrana najčešći uzrok salmonele?,Piletina,Jaja,Sladoled,Riba,Piletina